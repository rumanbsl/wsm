'use strict';
/*eslint no-console: 0*/
const exec = require('child_process').exec;

if (process.platform === 'linux' || process.platform === 'darwin') {
  exec(['mv ./mobile/* ./'], (err, out, stdErr) => {
    if (err) console.log(stdErr);
  });
}

if (process.platform.match(/^win/)) {
  exec('xcopy mobile /e /y', (err, out, stdErr) => {
    console.log(out);
    if (err) console.log(stdErr);
  });

}

