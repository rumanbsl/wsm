const path = require('path');

const projectRoot = path.resolve(__dirname).replace('webpack-partials', '');
const nodeExternals = require('webpack-node-externals');
const plugins = require('./plugins');
const rules = require('./module');
const { production, development } = require('./env-variables');
const {alias} = require('./externals');
const renderer = {
  resolve: {alias},
  devtool: development ? 'eval' : false,

  entry: production ?
    [path.resolve(projectRoot, 'src', 'index')] :
  [
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    path.resolve(projectRoot, 'src', 'index')
  ],
  devServer: {
    hot: production ? false : true
  },
  output: {
    path: path.resolve(projectRoot, 'dist', 'desktop'),
    publicPath: production ? '' : 'http://localhost:8080/',
    filename: 'index.js'
  },
  target: 'electron-renderer',
  node: {
    __dirname: false,
    __filename: false
  },
  plugins: plugins,

  module: {
    rules: rules
  },
  cache: true
};

const main = {
  externals: production ? [nodeExternals(
    {
      whitelist: [/*'jquery', /^lodash/*/]
    }
  )] : {},
  entry: [path.resolve(projectRoot, 'src', 'electronMain')],
  output: {
    path: path.resolve(projectRoot, 'dist', 'desktop'),
    publicPath: '',
    filename: 'main.js'
  },
  target: 'electron-main',
  node: {
    __dirname: false,
    __filename: false
  },
  module: {
    rules: rules
  },
  plugins: plugins
};

module.exports = { renderer, main };

