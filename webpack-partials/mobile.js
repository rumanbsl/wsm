const path = require('path');
const projectRoot = path.resolve(__dirname).replace('webpack-partials', '');
const plugins = require('./plugins');
const rules = require('./module');
// const { externals, resolveLoader } = require('./externals');
const { production } = require('./env-variables');
const {alias} = require('./externals');
const phoneGap = {
  resolve: {alias},
  entry: path.resolve(projectRoot, 'src', 'index.js'),
  devServer: {
    hot: production ? false : true,
    port: 3003
  },
  target: 'web',
  output: {
    path: path.resolve(projectRoot, 'www'),
    filename: 'index.js'
  },
  module: {
    rules
  },
  plugins,
  cache: true
};

module.exports = phoneGap;