const env = {
  production: process.env.BUILD === 'production',
  development: process.env.BUILD === 'development',
  desktop: process.env.PLATFORM === 'desktop',
  mobile: process.env.PLATFORM === 'mobile',
  web: {
    back: process.env.PLATFORM === 'back',
    front: process.env.PLATFORM === 'front',
    build: process.env.PLATFORM === 'web'
  }
};

module.exports = {
  production: env.production,
  development: env.development,
  desktop: env.desktop,
  mobile: env.mobile,
  web: env.web,
};