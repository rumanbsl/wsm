const { production, development, mobile, desktop } = require('./env-variables');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const cssMinimized = {
  loader: 'css-loader',
  options: {
    minimize: true
  }
};
let rules = [
  {
    test: /\.vue$/,
    loader: 'vue-loader',
    options: {
      postcss: production ? [require('postcss-cssnext')()] : null,
      loaders: {
        extractCSS: production ? true : false,
        css: production ?
          ExtractTextWebpackPlugin.extract({
            use: cssMinimized,
            fallback: 'vue-style-loader'
          }) :
          'vue-style-loader!css-loader',
        sass: production ?
          ExtractTextWebpackPlugin.extract({
            use: [cssMinimized, 'sass-loader?indentedSyntax'],
            fallback: 'vue-style-loader'
          }) :
          ['vue-style-loader!css-loader!sass-loader?indentedSyntax'],
        scss: production ?
          ExtractTextWebpackPlugin.extract({
            use: [cssMinimized, 'sass-loader'],
            fallback: 'vue-style-loader'
          }) :
          ['vue-style-loader!css-loader!sass-loader']
      }
    }
  },
  {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
      loader: 'babel-loader',
      options: {
        presets: development ? ['env'] : ['babili', ['env']]
      }
    }
  },
  {
    test: /\.(png|jpg|jpeg|bmp|svg)$/,
    use: [
      {
        loader : 'url-loader',
        options : {
          name : './images/[name].[ext]',
          limit : 10000,
          publicPath: '.'
        }
      },
      {
        loader: 'image-webpack-loader',
        options: {
          mozjpeg: {
            quality: 65
          },
          pngquant: {
            quality: '10-20',
            speed: 4
          },
          svgo: {
            plugins: [
              {
                removeViewBox: false
              },
              {
                removeEmptyAttrs: false
              }
            ]
          },
          gifsicle: {
            optimizationLevel: 7,
            interlaced: false
          },
          optipng: {
            optimizationLevel: 7,
            interlaced: false
          }
        }
      }
    ],
    // exclude: /node_modules/
  },
  { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, use: ['url?limit=10000&mimetype=application/font-woff&name=./fonts/[name]-[hash].[ext]'/*, exclude: /node_modules/*/] },
  { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, use: ['url?limit=10000&mimetype=application/font-woff&name=./fonts/[name]-[hash].[ext]'/*, exclude: /node_modules/*/] },
  { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: ['url?limit=10000&mimetype=application/octet-stream&name=./fonts/[name]-[hash].[ext]'/*, exclude: /node_modules/*/] },
  { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: ['file?name=./fonts/[name]-[hash].[ext]'] }
];


if (development) {
  rules.push(
    {
      enforce: 'pre',
      test: /\.(js|vue)$/,
      exclude: /node_modules/,
      loader: 'eslint-loader',
      options: {
        formatter: require('eslint/lib/formatters/stylish'),
        emitError: true
      }
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
            url: false
          }
        }
      ],
      /*exclude: /node_modules/*/
    },
    {
      test: /\.(sass|scss)$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
            url:false
          }
        },
        'resolve-url-loader',
        'sass-loader'
      ],
      /*exclude: /node_modules/*/
    }
  );
}

if (production) {
  rules.push(
    {
      test: /\.css$/,
      use: ExtractTextWebpackPlugin.extract([
        {
          loader: 'css-loader',
          options: {
            minimize: true
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            plugins: function () {
              return [
                require('postcss-cssnext')()
              ];
            }
          }
        }]
      ),
      /*exclude: /node_modules/*/
    },
    {
      test: /\.(sass|scss)$/,
      use: ExtractTextWebpackPlugin.extract([
        {
          loader: 'css-loader',
          options: {
            minimize: true
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            plugins: function () {
              return [
                require('postcss-cssnext')()
              ];
            }
          }
        },
        'resolve-url-loader',
        'sass-loader']
      ),
      /*exclude: /node_modules/*/
    }
  );
}

module.exports = rules;