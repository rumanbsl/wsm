const path = require('path');
const {production} = require('./env-variables');
const dir = (path.resolve(__dirname).replace('webpack-partials',''));
const vue = production ? path.resolve(dir, 'node_modules','vue','dist', 'vue.min.js') : path.resolve(dir, 'node_modules','vue','dist', 'vue.js');


module.exports = {
  alias: {
    vue
  }
};


