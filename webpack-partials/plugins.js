const { desktop, mobile, web, production, development } = require('./env-variables');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const path = require('path');
const glob = require('glob');
const PurifyCSSPlugin = require('purifycss-webpack');
//const projectRoot = path.resolve(__dirname).replace('webpack-partials', '');
let plugins = [

  new webpack.DefinePlugin({
    desktop: JSON.stringify(desktop),
    mobile: JSON.stringify(mobile),
    production: JSON.stringify(production),
    development: JSON.stringify(development),
    web: JSON.stringify(web),
  }),
  new HtmlWebPackPlugin({
    filename: 'index.html',
    template: './src/index.html',
    js: desktop && production ? '<script type="text/javascript" src="index.js"></script>'
      : '<script type="text/javascript" src="http://localhost:8080/index.js"></script>',
    link: desktop && production ? '<link rel="stylesheet" href="css/style.css">' : '<!---->',
    VueJS: development ? '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.2/vue.js"></script>' : '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.2/vue.min.js"></script>',
    inject: desktop ? false : true,
    hash: true,
    minify: {
      collapseWhitespace: production ? true : false
    }
  }),
  new FriendlyErrorsWebpackPlugin()
];


if (development) {
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new StyleLintPlugin({
      configFile: './.stylelintrc',
      files: ['src/**/*.vue', 'src/**/*.sass', 'src/**/*.scss']
    }),
    new WriteFilePlugin({
      test: /\.html$/,
      useHashIndex: true,
      log: false
    })
  );
}

if (production) {
  plugins.push(
    new ExtractTextPlugin({
      filename: 'css/style.css',
      allChunks: true
    }),
    new BabiliPlugin({
      simplify: false,
      minify: false
    })
    /*,
    new PurifyCSSPlugin({
      // Give paths to parse for rules. These should be absolute!
      paths: glob.sync(path.join(__dirname, '', 'src/*.{js,html,vue}')),
      minimize: true,
      purifyOptions: { info: true }
    })*/
  );
}

module.exports = plugins;
