/* global  mobile */
/*eslint no-console: 0*/
// common code entry point is in main, for clarity

import './Components/main';
import './images/imgs.jpg';
import './css/index.scss';
function inMobile () {

  document.addEventListener('deviceready', onDeviceReady.bind(this), false);

  function onDeviceReady () {
    // Handle the Cordova pause and resume events
    plugins(navigator);
    document.addEventListener('pause', onPause.bind(this), false);
    document.addEventListener('resume', onResume.bind(this), false);
  }

  function onPause () {
    // TODO: This application has been suspended. Save application state here.
  }

  function onResume () {
    // TODO: This application has been reactivated. Restore application state here.
  }
}
if (mobile) inMobile();

function plugins ({ camera }) {
  console.log('camera functioning: ', Boolean(camera));
}
