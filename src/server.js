/*eslint no-console:0*/
const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
app.use(express.static(__dirname));

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.get( '/',(req,res)=>{
  res.sendFile(__dirname + '/index.html');
});

app.get('/api', function (req, res) {
  const url = `http://api.eventful.com/json/events/search?...&app_key=SKwrFJX4G2hBzFJm&location=${req.query.city}`;
  fetch(url)
    .then(data => data.json())
    .then(data => res.json(data));
});

app.get('/api/pages', function (req, res) {
  const url = `http://api.eventful.com/json/events/search?...&app_key=SKwrFJX4G2hBzFJm&location=${req.query.city}&page_number=${req.query.page_number}`;
  fetch(url)
    .then(data => data.json())
    .then(data => res.json(data));
});



app.listen(process.env.PORT||5000, function(){console.log('listening on port 5000');});