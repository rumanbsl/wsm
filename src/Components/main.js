/*eslint no-console: 0*/
///*global web mobile */


import Vue from 'vue';

var VueMaterial = require('vue-material');
Vue.use(VueMaterial);

Vue.material.registerTheme('about', {
  primary: {
    color: 'indigo',
    hue: 'A200'
  },
  accent: {
    color: 'grey',
    hue: 300
  }
});
Vue.material.setCurrentTheme('about');
import Events from './Events.vue';

const app = new Vue({
  el: '.app',
  render: load => load(Events)
});
module.exports = app;

