const { app, BrowserWindow } = require('electron');
const path = require('path');
/*global production */
let win;
function createWindow () {
  win = new BrowserWindow({ width: 800, height: 600 });
  const fileurl = production ? path.join(__dirname, 'index.html') : 'http://localhost:8080';
  win.loadURL(fileurl);
  win.on('closed', () => {
    win = null;
  });
}
app.on('ready', createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});