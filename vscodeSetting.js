const userSettingJSON = // Place your settings in this file to overwrite default and user settings.
  {
    'editor.tabSize': 2,
    'eslint.autoFixOnSave': true,
    'eslint.enable': true,
    'files.exclude': {
      '**/.git': true,
      '**/.svn': true,
      '**/.hg': true,
      '**/CVS': true,
      '**/.DS_Store': true,
      'hooks': true,
      'plugins': true,
      '.bithound*': true,
      '.config*': true,
      'jsc*': true,
      'node_modules': true,
      'platforms': true,
      '.gitignore': true,
      'config*': true,
      '.style*': true,
      '.esl*': true,
      'movePh*': true
    }
  };

const workspaceSettingJSON = // Place your settings in this file to overwrite default and user settings.
  {
    'editor.tabSize': 2,
    'eslint.autoFixOnSave': true,
    'eslint.enable': true,
    'files.exclude': {
      '**/.git': true,
      '**/.svn': true,
      '**/.hg': true,
      '**/CVS': true,
      '**/.DS_Store': true,
      'hooks': true,
      'plugins': true,
      '.bithound*': true,
      '.config*': true,
      'jsc*': true,
      'node_modules': true,
      'platforms': true,
      '.gitignore': true,
      'config*': true,
      '.style*': true,
      '.esl*': true,
      'movePh*': true
    }
  };