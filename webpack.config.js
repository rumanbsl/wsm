const { main, renderer } = require('./webpack-partials/desktop');
const phoneGap = require('./webpack-partials/mobile');
const { back, front } = require('./webpack-partials/web');

let platform = [];
const { desktop, mobile, web } = require('./webpack-partials/env-variables');
if (desktop) {
  platform = [];
  platform.push(main, renderer);
}

if (mobile) {
  platform = [];
  platform.push(phoneGap);
}

if (web.build) {
  platform = [];
  platform.push(front, back);
}

if (web.front) {
  platform = [];
  platform.push(front);
}

if (web.back) {
  platform = [];
  platform.push(back);
}

module.exports = platform;